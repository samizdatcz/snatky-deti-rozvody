function count_n_show(button) {

	d3.csv("data.csv", function(d) {

		return {
			rok: +d.rok,
			proc: +d.procpodm,
			statistika: d.statistika,
			pohlavi: d.pohlavi,
			vek: d.vek
		};

	}, function(data) {

		var skupina,
			skupinaSvatba,
			skupinaNarozeni,
			skupinaRozvod;

		var svatba2015,
			svatba1989,
			svatba1950,
			narozeni2015,
			narozeni1989,
			narozeni1950,
			rozvod2015,
			rozvod1989,
			rozvod1950,
			umrti2015,
			umrti1989,
			umrti1950;

		var colors = ["#e41a1c", "#377eb8", "#4daf4a", "#ffbf00"];

		var vek,
			pohlavi;

		vek = Number(document.getElementById("vek").value);
		pohlavi = document.getElementById("pohlavi").value;

		if(vek == 0) {
			skupina = '0';
		} else if (vek >= 1 && vek <= 4) {
			skupina = '1-4';
		} else if (vek >= 5 && vek <= 9) {
			skupina = '5-9';
		} else if (vek >= 10 && vek <= 14) {
			skupina = '10-14';
		} else if (vek >= 15 && vek <= 19) {
			skupina = '15-19';
		} else if (vek >= 20 && vek <= 24) {
			skupina = '20-24';
		} else if (vek >= 25 && vek <= 29) {
			skupina = '25-29';
		} else if (vek >= 30 && vek <= 34) {
			skupina = '30-34';
		} else if (vek >= 35 && vek <= 39) {
			skupina = '35-39';
		} else if (vek >= 40 && vek <= 44) {
			skupina = '40-44';
		} else if (vek >= 45 && vek <= 49) {
			skupina = '45-49';
		} else if (vek >= 50 && vek <= 54) {
			skupina = '50-54';
		} else if (vek >= 55 && vek <= 59) {
			skupina = '55-59';
		} else if (vek >= 60 && vek <= 64) {
			skupina = '60-64';
		} else if (vek >= 65 && vek <= 69) {
			skupina = '65-69';
		} else if (vek >= 70 && vek <= 74) {
			skupina = '70-74';
		} else if (vek >= 75 && vek <= 79) {
			skupina = '75-79';
		} else if (vek >= 80 && vek <= 84) {
			skupina = '80-84';
		} else if (vek >= 85) {
			skupina = '85+'
		}

		svatba2015 = data.filter(function(d) {
			if (vek >= 55) {skupinaSvatba = '55+'} else {skupinaSvatba = skupina}
			return (d.statistika == 'svatba' && d.rok == '2015' && d.pohlavi == pohlavi && d.vek == skupinaSvatba);
		})[0].proc

		svatba1989 = data.filter(function(d) {
			if (vek >= 55) {skupinaSvatba = '55+'} else {skupinaSvatba = skupina}
			return (d.statistika == 'svatba' && d.rok == '1989' && d.pohlavi == pohlavi && d.vek == skupinaSvatba);
		})[0].proc

		svatba1950 = data.filter(function(d) {
			if (vek >= 55) {skupinaSvatba = '55+'} else {skupinaSvatba = skupina}
			return (d.statistika == 'svatba' && d.rok == '1950' && d.pohlavi == pohlavi && d.vek == skupinaSvatba );
		})[0].proc

		narozeni2015 = data.filter(function(d) {
			if (vek >= 50) {skupinaNarozeni = '50+'} else {skupinaNarozeni = skupina}
			return (d.statistika == 'narozeni' && d.rok == '2015' && d.pohlavi == 'zena' && d.vek == skupinaNarozeni );
		})[0].proc

		narozeni1989 = data.filter(function(d) {
			if (vek >= 50) {skupinaNarozeni = '50+'} else {skupinaNarozeni = skupina}
			return (d.statistika == 'narozeni' && d.rok == '1989' && d.pohlavi == 'zena' && d.vek == skupinaNarozeni );
		})[0].proc

		narozeni1950 = data.filter(function(d) {
			if (vek >= 50) {skupinaNarozeni = '50+'} else {skupinaNarozeni = skupina}
			return (d.statistika == 'narozeni' && d.rok == '1950' && d.pohlavi == 'zena' && d.vek == skupinaNarozeni );
		})[0].proc

		rozvod2015 = data.filter(function(d) {
			if (vek >= 60) {skupinaRozvod = '60+'} else {skupinaRozvod = skupina}
			return (d.statistika == 'rozvod' && d.rok == '2015' && d.pohlavi == pohlavi && d.vek == skupinaRozvod );
		})[0].proc

		rozvod1989 = data.filter(function(d) {
			if (vek >= 60) {skupinaRozvod = '60+'} else {skupinaRozvod = skupina}
			return (d.statistika == 'rozvod' && d.rok == '1989' && d.pohlavi == pohlavi && d.vek == skupinaRozvod );
		})[0].proc

		rozvod1950 = data.filter(function(d) {
			if (vek >= 60) {skupinaRozvod = '60+'} else {skupinaRozvod = skupina}
			return (d.statistika == 'rozvod' && d.rok == '1950' && d.pohlavi == pohlavi && d.vek == skupinaRozvod );
		})[0].proc

		umrti2015 = data.filter(function(d) {
			return (d.statistika == 'umrti' && d.rok == '2015' && d.pohlavi == pohlavi && d.vek == skupina );
		})[0].proc

		umrti1989 = data.filter(function(d) {
			return (d.statistika == 'umrti' && d.rok == '1989' && d.pohlavi == pohlavi && d.vek == skupina );
		})[0].proc

		umrti1950 = data.filter(function(d) {
			return (d.statistika == 'umrti' && d.rok == '1950' && d.pohlavi == pohlavi && d.vek == skupina );
		})[0].proc

		if(button == "ja") {
			var text = '<h3>Pravděpodobnost, že vás právě letos čeká*</h3>';
			text += '<div id="odpoved">První svatba: <strong2015>' + svatba2015 + ' %</strong2015></div>';
			text += '<div id="detail">V roce 1989 by to bylo <strong1989>' + svatba1989 + ' %</strong1989>, v roce 1950 pak <strong1950>' + svatba1950 + ' %</strong1950>.</div>';
			if (pohlavi == 'zena') {
				text += '<div id="detail">Až do poloviny devadesátých let se průměrný věk svobodné nevěsty pohyboval kolem 22 let. Od té doby stoupl až k hranici 30 let.</div>'
			} else {
				text += '<div id="detail">U mužů se držel průměrný věk prvního sňatku až do poloviny devadesátých let na 24 letech. Dnes je to 32 let.</div>'
			}
			if (pohlavi == 'zena') {
				text += '<div id="odpoved">První dítě: <strong2015>' + narozeni2015 + ' % </strong2015></div>';
				text += '<div id="detail">V roce 1989 by to bylo <strong1989>' + narozeni1989 + ' %</strong1989>, v roce 1950 pak <strong1950>' + narozeni1950 + ' %</strong1950>.</div>';
				text += '<div id="detail">Věk prvorodiček zůstával od padesátých do poloviny devadesátých let poměrně stabilní. Během následujících deseti let přišla prudká změna: průměrný věk prvorodiček stoupl z 23 až nad 28 let. Podíl žen s prvním dítětem ve věku 15-19 let mezi všemi prvorodičkami klesl na pětinu, v kategorii 30-34 let desetkrát vyrostl. V posledních pěti letech se změny téměř zastavily.</div>'
				text += '<div id="detail">Mimochodem, průměrný věk všech rodiček – tedy nejen prvního, ale i všech dalších dětí – je dnes stejný jako v roce 1920, tedy 30 let. Je to tím, že dřív měly ženy více dětí, část z nich ve vyšším věku.</div>';
			} else {
				text += '<div id="odpoved">První dítě (pokud byste byl žena ve svém věku): <strong2015>' + narozeni2015 + ' %</strong2015></div>'
				text += '<div id="detail">V roce 1989 by to bylo <strong1989>' + narozeni1989 + ' %</strong1989>, v roce 1950 pak <strong1950>' + narozeni1950 + ' %</strong1950>.</div>';
				text += '<div id="detail">Věk dítěte známe pouze u ženatých mužů – věk svobodných otců statistici evidují až v posledních desetu letech. I mužům proto nabízíme data o věku prvorodiček.</div>';
				text += '<div id="detail">Z příbuzných statistik vyplývá, že otec dítěte bývá o dva až tři roky starší než matka. Po třicítce se věkový rozdíl snižuje, tj. častěji mají dítě podobně staří rodiče.</div>';
			}
			text += '<div id="odpoved">Rozvod (u sezdaných): <strong2015>' + rozvod2015 + ' %</strong2015></div>';
			text += '<div id="detail">V roce 1989 by to bylo <strong1989>' + rozvod1989 + ' %</strong1989>, v roce 1950 pak <strong1950>' + rozvod1950 + ' %</strong1950>.</div>';
			text += '<div id="detail">Statistici sledují také příčiny rozvodu. Dnes je suverénně nejčastější příčinou <i>rozdíl povah, názorů a zájmů</i>, v minulosti ovšem byly důvody pestřejší – nebo evidence poctivější. V polovině 80. let udávalo 16 procent žen i mužů jako příčinu rozvodu nevěru. Ženám také na mužích často vadil alkoholismus (13 procent rozvodů), nezájem o rodinu (8 procent) a „zlé nakládání“ (5 procent). Muži konkrétní příčiny k rozvodu obvykle neuváděli. Sexuální neshody stály za třemi procenty rozvodů.</div>';
			text += '<div id="odpoved">Úmrtí: <strong2015> ' + umrti2015 + ' % </strong2015></div>';
			text += '<div id="detail">V roce 1989 by to bylo <strong1989>' + umrti1989 + ' %</strong1989>, v roce 1950 pak <strong1950>' + umrti1950 + ' %</strong1950>.</div>';
			if (pohlavi == 'zena') {
				text += '<div id="detail">Poznámku, že na 85 let je to vachrlatý. Ženy mají oproti mužům zhruba do sedmdesáti let třetinovou až poloviční úmrtnost; oproti nim například nemají vyšší úmrtnost v pubertě. Do menopauzy jsou také chráněny hormony před srdečním selháním. Nevýhodou delšího života jsou ovšem častější duševní poruchy.</div>'
			} else {
				text += '<div id="detail">Poznámku, že na 85 let je to vachrlatý. Riziko úmrtí je u mužů vyšší přibližně mezi šestnáctým a pětadvacátým rokem, kdy jsou nejčastější příčinou úmrtí sebevraždy a nehody. Po čtyřicátém roce převáží nemoci srdce, po šedesátém roce přibývá mozková mrtvice.</div>'
			}
			if ((skupinaNarozeni == skupina) && (skupinaSvatba == skupina) && (skupinaRozvod == skupina)) {
				text += '<div id="poznamka">* Výpočet pravděpodobnosti v souladu se <a href="https://www.czso.cz/csu/czso/demograficka-prirucka-2015">zdrojovými daty ČSÚ</a> obvykle provádíme pro pětileté věkové skupiny, vy jste ve skupině ' + skupina + ' let.</div>'
			} else {
				text += '<div id="poznamka">* Výpočet pravděpodobnosti v souladu se <a href="https://www.czso.cz/csu/czso/demograficka-prirucka-2015">zdrojovými daty ČSÚ</a> obvykle provádíme pro pětileté věkové skupiny, vy jste ve skupinách:</div>'
				text += '<div id="poznamka">První svatba: ' + skupinaSvatba + ' let, první dítě: ' + skupinaNarozeni + ' let, rozvod: ' + skupinaRozvod + ' let, úmrtí: ' + skupina + ' let.</div>'
			}

			$("#result").html(text);
			}

		if(button == "ostatni") {

			var text = '<div id="chart1"><h3 id="charttitle">První svatba</h3></div>';
			text += '<div id="chart2"><h3 id="charttitle">První dítě</h3></div>';
			text += '<div id="chart3"><h3 id="charttitle">Rozvod</h3></div>';
			text += '<div id="chart4"><h3 id="charttitle">Úmrtí</h3></div>';
			$("#result").html(text);

			var w = 250,
				h = 200,
				padding = 30,
				paddingLeft = 50,
				paddingRight = 20;

			var datasvatba,
				datanarozeni,
				datarozvod,
				dataumrti;

			datasvatba = data.filter(function(d) {
				return (d.statistika == 'svatba' && d.rok == '2015' && d.pohlavi == pohlavi );
			})

			datanarozeni = data.filter(function(d) {
				return (d.statistika == 'narozeni' && d.rok == '2015' && d.pohlavi == 'zena' );
			})

			datarozvod = data.filter(function(d) {
				return (d.statistika == 'rozvod' && d.rok == '2015' && d.pohlavi == pohlavi );
			})

			dataumrti = data.filter(function(d) {
				return (d.statistika == 'umrti' && d.rok == '2015' && d.pohlavi == pohlavi );
			})

// první graf, svatba

			var xScale = d3.scale.ordinal()
				.domain(datasvatba.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * d3.max(datasvatba, function(d) {
					return d.proc
				})])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 4 || i == 6 || i == 8 || i == 12);
			}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 + " %"; });

			var svg1 = d3.select("#chart1")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			var bar = svg1.selectAll("g")
				.data(datasvatba)
				.enter()
				.append("g")
				.attr("transform", function(d, i) {
					return "translate(" + (xScale(d.vek) - 7) + ", " + yScale(d.proc) + ")";
				});

			bar.append("rect")
				.attr("width", (w - 2 * padding) / datasvatba.length - 1)
				.attr("height", function(d) {
					return yScale(0) - yScale(d.proc);
				})
				.attr("fill", colors[0])
				.on("mouseover", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", .2)
				})
				.on("mouseout", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", 1)
				})

			bar.on("mouseover", function(d, i) {
				d3.select(this)
					.attr("data-tooltip", function(d) {
						return d.vek + " let: " + d.proc + " %";
					});

				})
				.on("mouseout", function(d) {
					svg1.selectAll("text.legend")
						.transition()
						.duration(200)
						.style("opacity", 0)
						.remove();
				})

			svg1.append("rect")
				.attr("x", xScale(skupinaSvatba))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg1.append("circle")
				.attr("cx", xScale(skupinaSvatba))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg1.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupinaSvatba) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg1.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg1.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

			svg1.append("text")
				.attr("class", "axis")
				.attr("text-anchor", "middle")
				.attr("transform", "translate("+ (paddingLeft/2) +","+(h/2)+")rotate(-90)")
				.text("pravděpodobnost události");

// druhý graf, narození

			var xScale = d3.scale.ordinal()
				.domain(datanarozeni.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * d3.max(datanarozeni, function(d) {
					return d.proc
				})])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 4 || i == 6 || i == 8 || i == 11);
			}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var svg2 = d3.select("#chart2")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			var bar = svg2.selectAll("g")
				.data(datanarozeni)
				.enter()
				.append("g")
				.attr("transform", function(d, i) {
					return "translate(" + (xScale(d.vek) - 8) + ", " + yScale(d.proc) + ")";
				});

			bar.append("rect")
				.attr("width", (w - 2 * padding) / datanarozeni.length - 1)
				.attr("height", function(d) {
					return yScale(0) - yScale(d.proc);
				})
				.attr("fill", colors[0])
				.on("mouseover", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", .2)
				})
				.on("mouseout", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", 1)
				})

			bar.on("mouseover", function(d, i) {
				d3.select(this)
					.attr("data-tooltip", function(d) {
						return d.vek + " let: " + d.proc + " %";
					});
				})
				.on("mouseout", function(d) {
					svg2.selectAll("text.legend")
						.transition()
						.duration(200)
						.style("opacity", 0)
						.remove();
				})

			svg2.append("rect")
				.attr("x", xScale(skupinaNarozeni))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg2.append("circle")
				.attr("cx", xScale(skupinaNarozeni))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg2.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupinaNarozeni) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg2.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg2.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

// třetí graf, rozvod

			var xScale = d3.scale.ordinal()
				.domain(datarozvod.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * d3.max(datarozvod, function(d) {
					return d.proc
				})])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 4 || i == 7 || i == 10 || i == 13);
			}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var svg3 = d3.select("#chart3")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			var bar = svg3.selectAll("g")
				.data(datarozvod)
				.enter()
				.append("g")
				.attr("transform", function(d, i) {
					return "translate(" + (xScale(d.vek) - 7) + ", " + yScale(d.proc) + ")";
				});

			bar.append("rect")
				.attr("width", (w - 2 * padding) / datarozvod.length - 1)
				.attr("height", function(d) {
					return yScale(0) - yScale(d.proc);
				})
				.attr("fill", colors[0])
				.on("mouseover", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", .2)
				})
				.on("mouseout", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", 1)
				})

			bar.on("mouseover", function(d, i) {
				d3.select(this)
					.attr("data-tooltip", function(d) {
						return d.vek + " let: " + d.proc + " %";
					});
				})
				.on("mouseout", function(d) {
					svg3.selectAll("text.legend")
						.transition()
						.duration(200)
						.style("opacity", 0)
						.remove();
				})

			svg3.append("rect")
				.attr("x", xScale(skupinaRozvod))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg3.append("circle")
				.attr("cx", xScale(skupinaRozvod))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg3.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupinaRozvod) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg3.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg3.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

// čtvrtý graf, úmrtí

			var xScale = d3.scale.ordinal()
				.domain(dataumrti.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * d3.max(dataumrti, function(d) {
					return d.proc
				})])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 9 || i == 12 || i == 15 || i == 18);
			}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var svg4 = d3.select("#chart4")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			var bar = svg4.selectAll("g")
				.data(dataumrti)
				.enter()
				.append("g")
				.attr("transform", function(d, i) {
					return "translate(" + (xScale(d.vek) - 5) + ", " + yScale(d.proc) + ")";
				});

			bar.append("rect")
				.attr("width", (w - 2 * padding) / dataumrti.length - 1)
				.attr("height", function(d) {
					return yScale(0) - yScale(d.proc);
				})
				.attr("fill", colors[0])
				.on("mouseover", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", .2)
				})
				.on("mouseout", function(d) {
					d3.select(this)
						.transition()
						.duration(200)
						.style("opacity", 1)
				})

			bar.on("mouseover", function(d, i) {
				d3.select(this)
					.attr("data-tooltip", function(d) {
						return d.vek + " let: " + d.proc + " %";
					});
				})
				.on("mouseout", function(d) {
					svg4.selectAll("text.legend")
						.transition()
						.duration(200)
						.style("opacity", 0)
						.remove();
				})

			svg4.append("rect")
				.attr("x", xScale(skupina))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg4.append("circle")
				.attr("cx", xScale(skupina))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg4.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupina) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg4.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg4.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

		}

		if(button == "minulost") {

			var text = '<div id="chart1"><h3 id="charttitle">První svatba</h3></div>';
			text += '<div id="chart2"><h3 id="charttitle">První dítě</h3></div>';
			text += '<div id="chart3"><h3 id="charttitle">Rozvod</h3></div>';
			text += '<div id="chart4"><h3 id="charttitle">Úmrtí</h3></div>';
			$("#result").html(text);

			var w = 250,
				h = 200,
				padding = 30,
				paddingLeft = 50,
				paddingRight = 20;

			var datasvatba2015,
				datasvatba1989,
				datasvatba1950,
				datanarozeni2015,
				datanarozeni1989,
				datanarozeni1950,
				datarozvod2015,
				datarozvod1989,
				datarozvod1950,
				dataumrti2015,
				dataumrti1989,
				dataumrti1950;

			datasvatba2015 = data.filter(function(d) {
				return (d.statistika == 'svatba' && d.rok == '2015' && d.pohlavi == pohlavi );
			})

			datasvatba1989 = data.filter(function(d) {
				return (d.statistika == 'svatba' && d.rok == '1989' && d.pohlavi == pohlavi );
			})

			datasvatba1950 = data.filter(function(d) {
				return (d.statistika == 'svatba' && d.rok == '1950' && d.pohlavi == pohlavi );
			})

			datanarozeni2015 = data.filter(function(d) {
				return (d.statistika == 'narozeni' && d.rok == '2015' && d.pohlavi == 'zena' );
			})

			datanarozeni1989 = data.filter(function(d) {
				return (d.statistika == 'narozeni' && d.rok == '1989' && d.pohlavi == 'zena' );
			})

			datanarozeni1950 = data.filter(function(d) {
				return (d.statistika == 'narozeni' && d.rok == '1950' && d.pohlavi == 'zena' );
			})

			datarozvod2015 = data.filter(function(d) {
				return (d.statistika == 'rozvod' && d.rok == '2015' && d.pohlavi == pohlavi );
			})

			datarozvod1989 = data.filter(function(d) {
				return (d.statistika == 'rozvod' && d.rok == '1989' && d.pohlavi == pohlavi );
			})

			datarozvod1950 = data.filter(function(d) {
				return (d.statistika == 'rozvod' && d.rok == '1950' && d.pohlavi == pohlavi );
			})

			dataumrti2015 = data.filter(function(d) {
				return (d.statistika == 'umrti' && d.rok == '2015' && d.pohlavi == pohlavi );
			})

			dataumrti1989 = data.filter(function(d) {
				return (d.statistika == 'umrti' && d.rok == '1989' && d.pohlavi == pohlavi );
			})

			dataumrti1950 = data.filter(function(d) {
				return (d.statistika == 'umrti' && d.rok == '1950' && d.pohlavi == pohlavi );
			})

// první graf, svatba

			var xScale = d3.scale.ordinal()
				.domain(datasvatba2015.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * Math.max(d3.max(datasvatba2015, function(d) {return d.proc}),
											d3.max(datasvatba1989, function(d) {return d.proc}),
											d3.max(datasvatba1950, function(d) {return d.proc}))
				])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 4 || i == 6 || i == 8 || i == 12);
				}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var line = d3.svg.line()
				.interpolate("basis")
				.x(function(d) {
					return xScale(d.vek);
				})
				.y(function(d) {
					return yScale(d.proc);
				});

			var svg1 = d3.select("#chart1")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			svg1.append("path")
				.datum(datasvatba2015)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[0])

			svg1.append("path")
				.datum(datasvatba1989)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[1])

			svg1.append("path")
				.datum(datasvatba1950)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[2])

			svg1.append("rect")
				.attr("x", xScale(skupinaSvatba))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg1.append("circle")
				.attr("cx", xScale(skupinaSvatba))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg1.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupinaSvatba) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg1.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg1.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

			svg1.append("text")
				.attr("class", "legend")
				.attr("x", function(d) {
					if (pohlavi == 'zena') {return xScale("35-39")} else {return xScale("40-44")}
				})
				.attr("y", yScale(1.7))
				.text("2015")
				.style("stroke", colors[0])

			svg1.append("text")
				.attr("class", "legend")
				.attr("x", function(d) {
					if (pohlavi == 'zena') {return xScale("1-4")} else {return xScale("5-9")}
				})
				.attr("y", yScale(2))
				.text("1989")
				.style("stroke", colors[1])

			svg1.append("text")
				.attr("class", "legend")
				.attr("x", function(d) {
					if (pohlavi == 'zena') {return xScale("30-34")} else {return xScale("30-34")}
				})
				.attr("y", yScale(6))
				.text("1950")
				.style("stroke", colors[2])

			svg1.append("text")
				.attr("class", "axis")
				.attr("text-anchor", "middle")
				.attr("transform", "translate("+ (paddingLeft/2) +","+(h/2)+")rotate(-90)")
				.text("pravděpodobnost události");

// druhý graf, dítě

			var xScale = d3.scale.ordinal()
				.domain(datanarozeni2015.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * Math.max(d3.max(datanarozeni2015, function(d) {return d.proc}),
											d3.max(datanarozeni1989, function(d) {return d.proc}),
											d3.max(datanarozeni1950, function(d) {return d.proc}))
				])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 4 || i == 6 || i == 8 || i == 11);
				}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var line = d3.svg.line()
				.interpolate("basis")
				.x(function(d) {
					return xScale(d.vek);
				})
				.y(function(d) {
					return yScale(d.proc);
				});

			var svg2 = d3.select("#chart2")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			svg2.append("path")
				.datum(datanarozeni2015)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[0])

			svg2.append("path")
				.datum(datanarozeni1989)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[1])

			svg2.append("path")
				.datum(datanarozeni1950)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[2])

			svg2.append("rect")
				.attr("x", xScale(skupinaNarozeni))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg2.append("circle")
				.attr("cx", xScale(skupinaNarozeni))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg2.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupinaNarozeni) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg2.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg2.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

			svg2.append("text")
				.attr("class", "legend")
				.attr("x", xScale("40-44"))
				.attr("y", yScale(1.7))
				.text("2015")
				.style("stroke", colors[0])

			svg2.append("text")
				.attr("class", "legend")
				.attr("x", xScale("5-9"))
				.attr("y", yScale(4))
				.text("1989")
				.style("stroke", colors[1])

			svg2.append("text")
				.attr("class", "legend")
				.attr("x", xScale("25-29"))
				.attr("y", yScale(8))
				.text("1950")
				.style("stroke", colors[2])

// třetí graf, rozvod

			var xScale = d3.scale.ordinal()
				.domain(datarozvod2015.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * Math.max(d3.max(datarozvod2015, function(d) {return d.proc}),
											d3.max(datarozvod1989, function(d) {return d.proc}),
											d3.max(datarozvod1950, function(d) {return d.proc}))
				])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 4 || i == 7 || i == 10 || i == 13);
				}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var line = d3.svg.line()
				.interpolate("basis")
				.x(function(d) {
					return xScale(d.vek);
				})
				.y(function(d) {
					return yScale(d.proc);
				});

			var svg3 = d3.select("#chart3")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			svg3.append("path")
				.datum(datarozvod2015)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[0])

			svg3.append("path")
				.datum(datarozvod1989)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[1])

			svg3.append("path")
				.datum(datarozvod1950)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[2])

			svg3.append("rect")
				.attr("x", xScale(skupinaRozvod))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg3.append("circle")
				.attr("cx", xScale(skupinaRozvod))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg3.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupinaRozvod) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg3.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg3.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

			svg3.append("text")
				.attr("class", "legend")
				.attr("x", function(d) {
					if (pohlavi == 'zena') {return xScale("55-59")} else {return xScale("55-59")}
				})
				.attr("y", function(d) {
					if (pohlavi == 'zena') {return yScale(12)} else {return yScale(16)}
				})
				.text("2015")
				.style("stroke", colors[0])

			svg3.append("text")
				.attr("class", "legend")
				.attr("x", function(d) {
					if (pohlavi == 'zena') {return xScale("55-59")} else {return xScale("55-59")}
				})
				.attr("y", yScale(3))
				.text("1989")
				.style("stroke", colors[1])

			svg3.append("text")
				.attr("class", "legend")
				.attr("x", function(d) {
					if (pohlavi == 'zena') {return xScale("30-34")} else {return xScale("30-34")}
				})
				.attr("y", yScale(4))
				.text("1950")
				.style("stroke", colors[2])

// čtvrtý graf, úmrtí

			var xScale = d3.scale.ordinal()
				.domain(dataumrti2015.map(function(d) {
					return d.vek;
				}))
				.rangePoints([paddingLeft, w - paddingRight]);

			var yScale = d3.scale.linear()
				.domain([0, 1.1 * Math.max(d3.max(dataumrti2015, function(d) {return d.proc}),
											d3.max(dataumrti1989, function(d) {return d.proc}),
											d3.max(dataumrti1950, function(d) {return d.proc}))
				])
				.range([h - padding, padding]);

			var xAxis = d3.svg.axis()
				.orient("bottom")
				.scale(xScale)
				.tickValues(xScale.domain().filter(function(d, i) {
					return (i == 0 || i == 9 || i == 12 || i == 15 || i == 18);
				}))

			var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickValues(yScale.domain().filter(function(d, i) {
					return (i == 0 || i == 1);
				}))
				.tickFormat(function(d) { return parseInt(10 * d, 10)/10 +  " %"; });

			var line = d3.svg.line()
				.interpolate("basis")
				.x(function(d) {
					return xScale(d.vek);
				})
				.y(function(d) {
					return yScale(d.proc);
				});

			var svg4 = d3.select("#chart4")
				.append("svg")
				.attr("width", w)
				.attr("height", h);

			svg4.append("path")
				.datum(dataumrti2015)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[0])

			svg4.append("path")
				.datum(dataumrti1989)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[1])

			svg4.append("path")
				.datum(dataumrti1950)
				.attr("class", "line")
				.attr("d", line)
				.style("stroke", colors[2])

			svg4.append("rect")
				.attr("x", xScale(skupina))
				.attr("y", padding + 10)
				.attr("width", 0.5)
				.attr("height", h - 2 * padding - 10)
				.attr("r", 10)
				.style("fill", "black")

			svg4.append("circle")
				.attr("cx", xScale(skupina))
				.attr("cy", padding + 10)
				.attr("r", 3)
				.style("fill", "black")

			svg4.append("text")
				.attr("class", "legend")
				.attr("x", xScale(skupina) + 8)
				.attr("y", padding + 10)
				.text("VY")
				.style("stroke", "black")

			svg4.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);

			svg4.append("g")
				.attr("class", "axis")
				.attr("transform", "translate(" + paddingLeft + ",0)")
				.call(yAxis);

			svg4.append("text")
				.attr("class", "legend")
				.attr("x", xScale("80-84"))
				.attr("y", yScale(4))
				.text("2015")
				.style("stroke", colors[0])

			svg4.append("text")
				.attr("class", "legend")
				.attr("x", xScale("60-64"))
				.attr("y", yScale(10))
				.text("1989")
				.style("stroke", colors[1])

			svg4.append("text")
				.attr("class", "legend")
				.attr("x", xScale("1-4"))
				.attr("y", yScale(5))
				.text("1950")
				.style("stroke", colors[2])
			}
		})
	}

d3.select("#fallback").remove();

new Tooltip().watchElements()

$("#ja").click(function() {
	count_n_show("ja")
});

$("#ostatni").click(function() {
	count_n_show("ostatni")
});

$("#minulost").click(function() {
	count_n_show("minulost")
});